import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import * as screenNames from '../screenNames';

const Drawer = createDrawerNavigator();

// screen: Home & About
import Home from '../../screens/Home';
import About from '../../screens/About';

export default function(props){
    return (
        <Drawer.Navigator>
            <Drawer.Screen name={screenNames.HOME_SCREEN} component={Home} options={{title: 'Home'}} />
            <Drawer.Screen name={screenNames.ABOUT_SCREEN} component={About} options={{title: 'About'}} />
        </Drawer.Navigator>
    );
}