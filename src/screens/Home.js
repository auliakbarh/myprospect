import React from 'react';
import {View, Text} from 'react-native';

import * as screenNames from '../navigation/screenNames';

export default function(props){
    return (
        <View>
            <Text onPress={() => props.navigation.navigate(screenNames.ABOUT_SCREEN)}>Screen Home</Text>
        </View>
    )
}